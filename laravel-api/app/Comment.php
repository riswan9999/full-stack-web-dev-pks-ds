<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Comment extends Model
{
    protected $fillable = ['content'];
    protected $keyType='string';
    
    public $incrementing =  false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->id = Str::uuid();
            $model->post_id = Str::uuid();
        });

    }   
}
