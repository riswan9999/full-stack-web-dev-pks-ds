<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable =['username','email','name'];
    public $timestamps = false;
    protected $keyType='string';
    /* test */

    
    public $incrementing =  false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){

            $model->id = Str::uuid();
          
        });

    }
}
