<?php

namespace App;

// use App\Traits\UsesUuid; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
//    use UsesUuid;
   
    protected $fillable =['title','description'];
    protected $keyType='string';
    
    public $incrementing =  false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->id = Str::uuid();
        });

    }   
}
