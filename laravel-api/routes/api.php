<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/showposts','PostController@index');
Route::post('/createposts','PostController@store');

Route::get('/showcomments','CommentController@index');
Route::post('/createcomments','CommentController@store');

// Route::apiResource('posts','PostController');